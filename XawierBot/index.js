/* Copyright (C) 2021 xawier59.

Licensed under the  GPL-3.0 License;
you may not use this file except in compliance with the License.

XawierBot - xawier59
*/

module.exports = {
    Base: require('./Base'),
    Message: require('./Message'),
    StringSession: require('./StringSession'),
    ReplyMessage: require('./ReplyMessage'),
    Image: require('./Image'),
    Video: require('./Video')
};
